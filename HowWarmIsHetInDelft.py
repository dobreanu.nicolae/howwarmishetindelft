#!/usr/bin/python3

#compatibility set for python2
from __future__ import print_function
try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

import requests,re
from bs4 import BeautifulSoup

#extract temperature from static html content
def get_temp(url):
    content = requests.get(url)
    soup = BeautifulSoup(content.text, 'html.parser')
    row = str(soup.find('tr'))
    try:
        result=int(round(float(re.search(r'\d+\.\d+', row).group(0))))
        return result
    except Exception as e:
        print ("Cannot extract temperature!")
        print ("Try runing the flask_serverlication outside the container.")
        print (e)
        return False
		
#check if page exists
def check_for_page(url):
    try:
        request = requests.get(url)
        if request.status_code == 200:
            return True
    except Exception as e: 
        #print (e)
        return False

#extract html code from url
def get_html_from_url(url):
    page = urlopen(url)
    html_bytes = page.read()
    html = html_bytes.decode("utf-8")
    html=html.split()
    return html

#parse and traceback to source
def traceback_url(url):
    for i in get_html_from_url(url):
        if ("delft" in i) and ("www" in i):
            i=i.replace("href=","").replace('"','')
            if check_for_page(i):
                #print (i) #for debug purpose
                temp=get_temp(i)
                break
    return temp

#main function with hardcoded url
def return_temp(): 
    url="https://www.weerindelft.nl/"
    temp = traceback_url(url)
    if temp:
        temp = str(temp) + " degrees Celsius"
        print (temp)
    else:
        print (temp)
    return str(temp)

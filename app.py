from flask import Flask
import HowWarmIsHetInDelft

flask_server = Flask(__name__)

#flask decorator
@flask_server.route("/")
def display():
        return HowWarmIsHetInDelft.return_temp()

#start server on localhost 0.0.0.0 (default port 5000)
if __name__ == "__main__":
    flask_server.run(host='0.0.0.0')
